import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import router from './routes';

export const App = () => {
    return (
        <div className='App'>
            <RouterProvider router={router}></RouterProvider>
        </div>
    );
};

export default App;
