import { FC } from 'react';
import classes from './home.module.css';
import classnames from 'classnames';

const Home: FC = () => {
    return (
        <div className={classes.grid}>
            <div className={classnames([classes.title, classes.flex])}>
                <h1>Hi! I'm Gabriel.</h1>
                <p>I'm a software developer based in the Netherlands</p>
            </div>
            <div className={classnames([classes.subtitle, classes.flex])}>Check out my work!</div>
            <div className={classnames([classes.flex, classes.button])}>
                <button>Here</button>
            </div>
        </div>
    );
};

export default Home;
